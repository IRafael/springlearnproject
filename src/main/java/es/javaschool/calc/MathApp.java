package es.javaschool.calc;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import es.javaschool.calc.drivers.cmd.Driver;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class MathApp implements ApplicationListener<ContextRefreshedEvent> {

	private final Driver driver;

	public void start() {
		driver.open();
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		start();
	}
}

package es.javaschool.calc.adapters;

public interface Controller<T, R> {

	void acceptArg(T arg1);

	R process(T operation);
}

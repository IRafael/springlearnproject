package es.javaschool.calc.adapters.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import es.javaschool.calc.adapters.Controller;
import es.javaschool.calc.business_rules.MathUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class MathController implements Controller<String, String> {

	@Qualifier("multiply")
	private final MathUseCase<Integer, Integer, Integer> multiplyUC;
	@Qualifier("sum")
	private final MathUseCase<Integer, Integer, Integer> sumUC;
	@Qualifier("divide")
	private final MathUseCase<Integer, Integer, Integer> divideUC;
	@Qualifier("diff")
	private final MathUseCase<Integer, Integer, Integer> diffUC;

	private final List<Integer> args = new ArrayList<>();

	public void acceptArg(String arg) {
		args.add(Integer.parseInt(arg));
	}

	@Override
	public String process(String operation) {

		switch (operation) {
		case "/":
			return String.valueOf(divideUC.apply(args.get(0), args.get(1)));
		case "*":
			return String.valueOf(multiplyUC.apply(args.get(0), args.get(1)));
		case "+":
			return String.valueOf(sumUC.apply(args.get(0), args.get(1)));
		default:
			return String.valueOf(diffUC.apply(args.get(0), args.get(1)));
		}
	}
}

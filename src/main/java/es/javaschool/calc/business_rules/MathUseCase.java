package es.javaschool.calc.business_rules;

public interface MathUseCase<T, T1, R> {
	R apply(T arg, T1 arg1);
}

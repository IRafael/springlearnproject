package es.javaschool.calc.business_rules.impl;

import org.springframework.stereotype.Component;

import es.javaschool.calc.business_rules.MathUseCase;

@Component("divide")
public class DivideUseCase implements MathUseCase<Integer, Integer, Integer> {
	@Override
	public Integer apply(Integer arg, Integer arg1) {
		return arg / arg1;
	}
}
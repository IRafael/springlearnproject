package es.javaschool.calc.business_rules.impl;

import org.springframework.stereotype.Component;

import es.javaschool.calc.business_rules.MathUseCase;

@Component("multiply")
public class MultiplyUseCase implements MathUseCase<Integer, Integer, Integer> {
	@Override
	public Integer apply(Integer arg, Integer arg1) {
		return arg * arg1;
	}
}

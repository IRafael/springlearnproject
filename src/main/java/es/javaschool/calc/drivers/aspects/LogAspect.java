package es.javaschool.calc.drivers.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Aspect
@Profile("spain")
@Component
public class LogAspect {

	@Pointcut("execution(public Integer es.javaschool.calc.business_rules.impl.DiffUseCase.apply(Integer, Integer))")
	private void allPublicMethods() {
	}

	@Before("allPublicMethods()")
	public void beforeAdvice() {
		System.out.println("I'm running before call apply DiffUseCase method");
	}

	@After("allPublicMethods()")
	public void afterAdvice() {
		System.out.println("I'm running after call apply DiffUseCase method");
	}

}

package es.javaschool.calc.drivers.cmd;

public interface Driver {

	void open();
}

package es.javaschool.calc.drivers.cmd.impl;

import java.util.List;
import java.util.Scanner;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.javaschool.calc.adapters.Controller;
import es.javaschool.calc.drivers.cmd.Driver;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class CMDDriver implements Driver {

	@Value("${hint}")
	private String RESULT_IS = "Result is ";

	private final Controller<String, String> controller;

	private final Scanner scn;

	private final List<String> terminalOperations;

	@PostConstruct
	void init() {
		if (terminalOperations.size() != 4) {
			throw new RuntimeException("Application don't support so much operations");
		}
	}

	@Override
	public void open() {
		while (true) {
			String input = scn.nextLine();
			if (terminalOperations.contains(input)) {
				System.out.println(RESULT_IS + controller.process(input));
				break;
			} else {
				controller.acceptArg(input);
			}
		}
	}
}

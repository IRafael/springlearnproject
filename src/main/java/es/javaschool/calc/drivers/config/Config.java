package es.javaschool.calc.drivers.config;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
public class Config {

	@Bean
	public Scanner scn() {
		return new Scanner(System.in);
	}

	@Bean
	public List<String> terminalOperations() {
		return List.of("*", "/", "+", "-");
	}

}
